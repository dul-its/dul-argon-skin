# frozen_string_literal: true

# spec/capybara_helper.rb
require 'capybara/rspec'

Capybara.register_driver :remote_selenium_headless do |app|
  options = Selenium::WebDriver::Chrome::Options.new
  options.add_argument('--headless=new')
  options.add_argument('--window-size=1280,800')
  options.add_argument('--no-sandbox')
  options.add_argument('--disable-dev-shm-usage')
  options.add_argument('--ignore-certificate-errors')
  options.add_argument('--allow-insecure-localhost')

  # Disable telemetry, clear up logs
  options.add_option('goog:loggingPrefs', { browser: 'OFF' })
  options.add_preference(:browser, loggingPrefs: { browser: 'OFF' })

  Capybara::Selenium::Driver.new(
    app,
    browser: :remote,
    url: "http://#{ENV.fetch('SELENIUM_HOST', nil)}:4444/wd/hub",
    options:
  )
end

# IP resets on each run, so we need to get it in order to view the page in the browser
selenium_app_host = ENV.fetch('SELENIUM_APP_HOST') do
  Socket.ip_address_list
        .find(&:ipv4_private?)
        .ip_address
end

Capybara.server_host = selenium_app_host
Capybara.server_port = 3000

RSpec.configure do |config|
  config.before(:each, :js, type: :feature) do
    Capybara.app_host = "http://#{Capybara.server_host}:#{Capybara.server_port}"
    Capybara.current_driver = :remote_selenium_headless
  end
end
