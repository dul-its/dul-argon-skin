# frozen_string_literal: true

require 'rails_helper'

# Selenium 4 info:
# https://www.selenium.dev/documentation/webdriver/troubleshooting/upgrade_to_selenium_4/

# rubocop:disable RSpec/ExampleLength
# rubocop:disable RSpec/NoExpectationExample

RSpec.feature 'Sample JS Tests', type: :feature do # Mark this as a feature test and...
  scenario '+++ User visits the home page and gets the H1', :js do # also mark this as a JS test to use Selenium
    visit '/'
    within('#content') do
      if has_selector?('h1.main-title', wait: 30)
        h1_text = find('h1.main-title').text
        puts "~~ H1 Text: #{h1_text}" # Duke University Libraries Catalog
      else
        puts '~~ H1 element not found'
      end
    end
  end

  scenario '+++ Check avaiability status', :js do
    # Visit the URL and output the title
    puts '~~ Visiting /catalog/DUKE006222698'
    visit '/catalog/DUKE006222698' # Harry Potter : the creature vault
    puts "Page title: #{page.title}"

    # Wait (60s) until the availability has updated, then get the text (we may need to play with this for CI/CD)
    if page.has_selector?('#holding-22779694140008501 .item-available', wait: 60)
      availability_text = find('#holding-22779694140008501 .item-available').text
      puts "~~ Availability status: #{availability_text}"
    else
      puts '~~ Availability status element not found.'
    end
  end

  scenario '+++ Click on "where to find it" link and check modal content', :js do
    visit '/catalog/DUKE006222698' # Harry Potter : the creature vault

    # Click on the link to open the modal
    first('a[aria-label="where to find it"]', wait: 30).click

    # Wait for the modal to appear
    expect(page).to have_selector('#blacklight-modal', wait: 30)

    # output the modal header
    within('#blacklight-modal', wait: 30) do
      if has_selector?('h4', wait: 30)
        modal_header_text = find('h4').text
        puts "~~ Modal Header: #{modal_header_text}"
      else
        puts '~~ Modal Header: <h4> element not found'
      end
    end
  end
end

# rubocop:enable RSpec/NoExpectationExample
# rubocop:enable RSpec/ExampleLength
