# frozen_string_literal: true

require 'rails_helper'

# rubocop:disable RSpec/MultipleMemoizedHelpers
describe SearchBuilder do
  subject { search_builder_class.new(processor_chain) }

  let(:search_builder_class) do
    Class.new(described_class)
  end

  let(:processor_chain) { [] }

  describe '#processor_chain' do
    let(:sb) { search_builder_class.new(CatalogController.new) }

    it 'adds the add_call_number_query_to_solr to the processor chain' do
      expect(sb.processor_chain).to include(:add_call_number_query_to_solr)
    end

    it 'adds the min_match_for_initial_articles to the processor chain' do
      expect(sb.processor_chain).to include(:min_match_for_initial_articles)
    end
  end

  describe '#min_match_for_initial_articles' do
    let(:solr_parameters) { Blacklight::Solr::Request.new }
    let(:blacklight_config) { CatalogController.blacklight_config.deep_copy }
    let(:user_params) { {} }
    let(:context) { CatalogController.new }
    let(:search_builder_class) do
      Class.new(SearchBuilder) do
        include Blacklight::Solr::SearchBuilderBehavior
        include TrlnArgon::ArgonSearchBuilder
      end
    end
    let(:search_builder) { search_builder_class.new(context) }

    before do
      builder_with_params.add_query_to_solr(solr_parameters)
      builder_with_params.min_match_for_initial_articles(solr_parameters)
    end

    context 'when title search has an initial articles' do
      let(:builder_with_params) do
        search_builder.with(q: 'The Medical Journal of Australia',
                            'search_field' => 'title')
      end

      it 'adjust the min match setting' do
        expect(solr_parameters[:mm]).to eq('4<95%')
      end
    end

    context 'when title search is without initial article' do
      let(:builder_with_params) do
        search_builder.with(q: 'Medical Journal of Australia',
                            'search_field' => 'title')
      end

      it 'do not adjust the min match setting' do
        expect(solr_parameters[:mm]).to be_nil
      end
    end

    context 'when advanced title search has an initial article' do
      let(:builder_with_params) do
        search_builder.with('title' => 'A Medical Journal of Australia',
                            'search_field' => 'advanced')
      end

      it 'adjust the min match setting' do
        expect(solr_parameters[:mm]).to eq('4<95%')
      end
    end

    context 'when all fields search has an initial article' do
      let(:builder_with_params) do
        search_builder.with(q: 'The Medical Journal of Australia',
                            'search_field' => 'all_fields')
      end

      it 'do not adjust the min match setting' do
        expect(solr_parameters[:mm]).to be_nil
      end
    end
  end
end
# rubocop:enable RSpec/MultipleMemoizedHelpers
