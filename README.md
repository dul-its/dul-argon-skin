# README

## RUN NATIVE (non-Docker)
Clone this project repo, make sure you have Postgres installed and running, then...
```bash
$ cd /path/to/dul-argon-skin
$ git checkout bl7-upgrade
$ [sudo] bundle update
$ rails s
```
## RUN IN DOCKER
1. Copy `docs/rails.env`, `docs/app.env`, and `docs/db.env` to the project root, then edit to taste.
  a. Pay close attention to the `DATABASE_URL` value in `rails.env` and make sure it matches what you have in `db.env`
2. Build the Docker image
3. Spin up the application stack (which includes Postgres)
  
```bash
$ docker compose up [--build]
```

## RUN IN TEST MODE
The test instance is configured to use [selenium/standalone-chrome](https://hub.docker.com/r/selenium/standalone-chrome) for any feature tests that require javascript. This creates a separate instance in the docker setup that runs under http://localhost:4444/. It's got it's own UI we can use to (in theory) view tests hapen in real-time, but for now I've configured things to run headlessly.

These tests need to have a certain syntax (`:feature` and `js: true`), like:
```bash
RSpec.feature 'some test', type: :feature do
  scenario 'some task', js: true do
    ...
  end
end
```

To start up the test instance, use:
```bash
$ docker compose -f docker-compose.test.yml up
```

## DEPLOYMENT

### Helm Chart
The project includes a `helm-chart` which defines the necessary components to 
deploy the application within a Kubernetes stack (or for us -- OIT's OpenShift environment).

#### Prerequisites
You'll need to install OpenShift Command Line Interface and Helm (will provide instructions soon)  
[Install oc and helm](https://gitlab.oit.duke.edu/devops/antsy/-/wikis/Installing-OpenShift-and-Helm)

#### TL;DR
```
$ oc login ... 
$ cd /path/to/dul-argon-skin
$ helm install -f helm-chart/values.yaml <release-name> <chart-directory>
```
  
For instance, to install a "pre" release instance...  
`$ helm upgrade --install -f helm-chart/values.yaml pre helm-chart/`  
  
The end result will be an application instance that includes an auto-generated hostname.  
If you want to assign a specific hostname to the application...  
```
$ helm upgrade --install -f helm-chart/values.yaml \
--set-string hostname=find-pre.library.duke.edu \
pre helm-chart\
```
  
Finally, if you need to specify the certificate and key...  
```
$ cd /path/to/dul-argon-skin
$ helm upgrade --install -f helm-chart/values.yaml --set-string hostname=find-pre.library.duke.edu \
--set-file tls.crt=find-pre.pem \
--set-file tls.key=find-pre.key \
--set-file tls.caCert=find-pre-interm.pem \
pre helm-chart/
```
  
DNS NOTE -- You, or a DevOps rep will need to create a CNAME record for your domain name (in this case `find-pre.library.duke.edu`) in OIT's "Also-Known-As" (AKA) service.  
  
The CNAME will need to be pointed at `os-node-lb-fitz.oit.duke.edu` in order to reach the OpenShift environment.  ...  

