# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'addressable', '~> 2.5'
gem 'argon_call_number_search',
    git: 'https://github.com/trln/argon_call_number_search', tag: 'v0.1.6'
gem 'better_errors', '>= 2.8.0'
gem 'blacklight', '~> 7.28'
gem 'blacklight-hierarchy', git: 'https://github.com/trln/blacklight-hierarchy', tag: 'v6.3.1'
gem 'bootstrap', '~> 4.0'
gem 'devise'
gem 'devise-guests', '~> 0.6'
gem 'execjs'
# ruby-git  >= 1.15.0 breaks our OKD builds; see "root_of_worktree" method
# which results in error "not in a git working tree".
gem 'git', '~> 1.14.0'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'lcsort', '~> 0.9'
gem 'loofah', '>= 2.3.1'
gem 'msgpack', '>= 1.7.0'
gem 'non-stupid-digest-assets'
gem 'pg'
gem 'puma'
gem 'rack-attack'
gem 'rack-cors'
gem 'rails', '~> 7.1'
gem 'sassc-rails'
gem 'sprockets', '~> 4.0'
gem 'terser'
gem 'trln_argon', git: 'https://github.com/trln/trln_argon', tag: 'v2.4.5'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'velocityjs-rails'
gem 'view_component', '~> 2.83.0'

group :development do
  gem 'listen'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'capybara'
  gem 'rails-controller-testing'
  gem 'rspec-rails'
  gem 'rubocop', '~> 1.30'
  gem 'rubocop-rspec'
  gem 'selenium-webdriver'
  gem 'solr_wrapper', '>= 0.3'
end
