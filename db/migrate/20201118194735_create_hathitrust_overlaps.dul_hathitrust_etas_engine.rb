class CreateHathitrustOverlaps < ActiveRecord::Migration[6.0]
  def change
    create_table :hathitrust_overlaps do |t|
      t.string :oclc_number
      t.string :bib_id
      t.string :access
      t.timestamps
    end
    add_index :hathitrust_overlaps, :bib_id
    add_index :hathitrust_overlaps, :access
  end
end
