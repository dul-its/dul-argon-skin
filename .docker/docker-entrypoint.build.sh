#!/bin/bash
set -e


#bundle exec rake db:environment:set RAILS_ENV=production
#bundle exec rake db:migrate RAILS_ENV=production 2>/dev/null || bundle exec rake db:setup RAILS_ENV=production

#exec bundle exec "$@"

check_bundle() {
  bundle check || bundle install -j "$(nproc)"
}

echo "RAILS_ENV is currently set to [${RAILS_ENV}]..."
case $1 in
  start)
    wait-for-it ${DATABASE_HOST:-db}:${DATABASE_PORT:-5432} -t 30 --
    echo "database is ready at port 5432..."
    
    if [ "$RAILS_ENV" == "production" ]; then
      bundle exec rake db:migrate 2>/dev/null || bundle exec rake db:setup
    else
      echo "Supposedly running database setup tasks..."
      bin/rails db:environment:set RAILS_ENV=development
      bin/rails db:setup RAILS_ENV=development
      bin/rails db:migrate RAILS_ENV=development
      #bundle exec rake db:migrate RAILS_ENV=development || bundle exec rake db:setup
      #rake db:setup 2>/dev/null
      #rake db:setup
      #rake db:migrate
    fi
    #if [ "$RAILS_ENV" == "development" ]; then
    #    #rake db:setup 2>/dev/null
    #    exec bundle exec rails db:environment:set RAILS_ENV=development
    #    exec bundle exec rails db:setup RAILS_ENV=development
    #    exec bundle exec rails db:migrate RAILS_ENV=development
    #fi
    exec bundle exec rails server -b 0.0.0.0 -p 3000
    ;;

  rspec)
    wait-for-it db:5432 -t 30 --
    exec bundle exec rspec spec
    ;;

  rubocop)
    exec bundle exec rubocop --cache false
    ;;

  update)
    exec bundle update
    ;;

  shell)
    exec /bin/bash
    ;;

  *)
    exec "$@"
    ;;
esac

echo "ready to go..."
exec "$@"
