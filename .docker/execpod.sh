#!/bin/bash

set -e

bundle exec rake db:environment:set RAILS_ENV=production
bundle exec rake db:migrate RAILS_ENV=production 2>/dev/null || bundle exec rake db:setup RAILS_ENV=production

# bundle exec rake assets:precompile
echo "[execpod.sh] Done with pod prep..."

bundle exec rails server -p 3000 -b 0.0.0.0
