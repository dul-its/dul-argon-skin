#!/bin/bash

set -e

check_bundle() {
  bundle check || bundle install -j "$(nproc)"
}

echo "RAILS_ENV is currently set to [${RAILS_ENV}]..."
case $1 in
  start)
    unset BUNDLE_PATH
    unset BUNDLE_BIN

    wait-for-it ${DATABASE_HOST:-db}:${DATABASE_PORT:-5432} -t 30 --
    echo "database is ready at port:5432..."

    if [ "$RAILS_ENV" == "production" ]; then
      bundle exec rake db:migrate 2>/dev/null || bundle exec rake db:setup
    else
      echo "Supposedly running database setup tasks..."
      rails db:environment:set RAILS_ENV=development
      rails db:setup RAILS_ENV=development
      rails db:migrate RAILS_ENV=development
      #bundle exec rake db:migrate RAILS_ENV=development || bundle exec rake db:setup
      #rake db:setup 2>/dev/null
      #rake db:setup
      #rake db:migrate
    fi
    exec bundle exec rails server -b 0.0.0.0 -p 3000
    ;;

  test)
    echo 'test mode!'
    # exec bundle exec rails db:migrate
    # exec bundle exec rails server -p 3000 -d -e test
    # exec bundle exec rake spec
    ;;

  update)
    exec bundle update
    ;;

  shell)
    exec /bin/bash
    ;;

  *)
    exec "$@"
    ;;
esac

echo "ready to go..."
exec "$@"
