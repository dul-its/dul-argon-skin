# Dockerfile
# Build instructions for dul-argon-skin
#
# This serves as a "build" playbook of sorts
FROM image-mirror-prod-registry.cloud.duke.edu/library/ruby:3.1

# Become the root user
USER 0

# Add any system packages here.
RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
     git \
     nodejs \
     npm \
     wait-for-it \
     shared-mime-info \
     rsync \
     vim \
	&& rm -rf /var/lib/apt/lists/*

# Now that NPM is installed, let's install Yarn
RUN npm install -g yarn

WORKDIR /usr/src/app
COPY Gemfile* ./
# RUN gem install bundler
RUN gem install bundler -v 2.4.22
RUN bundle install

COPY . .

# Since the /app folder (containing .git) will be owned
# by a different user from the OKD container's user...
RUN git config --global --add safe.directory /app

COPY .docker/cron.d/ /etc/cron.d/

# Production copies of these go into the container
# (NOTE: these used to be managed by the DevOps' ansible playbooks)
COPY .docker/blacklight.yml /usr/src/app/config/

RUN bundle exec rake assets:precompile
RUN bundle exec rake trln_argon:reload_code_mappings

RUN chmod -R a+rw /usr/src/app
RUN chown -R 1001:1001 /usr/src/app

COPY .docker/docker-entrypoint.build.sh /usr/local/bin/docker-entrypoint.sh
COPY .docker/execpod.sh /usr/local/bin/execpod.sh

# The idea here is to ensure the DevOps "deployment"
# version of 'database.yml' is included in the image,
# while allowing developers to use their own version
# locally with little to no disruption
COPY .docker/database.yml config/database.yml

# Ensure our 'entrypoint' script is executable...
RUN chmod a+x /usr/local/bin/docker-entrypoint.sh
RUN chmod a+x /usr/local/bin/execpod.sh

EXPOSE 9292
USER 1001

# CMD ["start"]
# ENTRYPOINT ["docker-entrypoint.sh"]
# CMD ["bundle", "exec", "rails", "server", "-p", "3000", "-b", "0.0.0.0"]

# CMD ["start"]
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["bash", "-c", "bundle exec rails db:migrate && bundle exec rails server -p 3000 -b 0.0.0.0"]

