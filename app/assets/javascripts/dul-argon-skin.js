$(document).ready(function() {

  /* Strip ids from document tools links, e.g., <a id="emailLink">. */
  /* DUL renders those multiple times per page (esp. for mobile).   */
  /* It's invalid HTML & is flagged as an accessibility violation.  */
  $('.doc-tools a').attr('id','');

  /* Ensure that ESC key closes Bootstrap dropdowns. */
  $(document).keyup(function(e) {
       if (e.keyCode == 27) { // escape key maps to keycode `27`
         $('.dropdown-toggle').dropdown('hide');
      }
  });

  /* If it's an item show page w/sidebar, activate stickiness and scrollspy */
  if ( $('#show-sidebar').length ) {

    $(window).scroll(function() {
      if ( $(document).scrollTop() > 130 ) {
        $("#show-sidebar-inner").addClass("sticky-stuck");
      } else {
        $("#show-sidebar-inner").removeClass("sticky-stuck");
      }
    });

    $('body').scrollspy({
      target: '#show-sidebar-inner',
      offset: 30
    });

  }

  // Smooth scroll to all anchors, esp. useful for sidebar menu

  $('a[href*="#"]:not([href="#"]):not([data-toggle]):not(".btn-hide")').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
      || location.hostname == this.hostname) {

      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 500);
        target.focus(); // Setting focus
        if (target.is(":focus")){ // Checking if the target was focused
          return false;
        } else {
          target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
          target.focus(); // Setting focus
        };
        return false;
      }
    }
  });

  // Smooth scroll to top
  $('a.back-to-top').click(function() {
    $('html,body').animate({
        scrollTop: 0
    }, 500);
    return false;
  });


  /* DUL masthead primary navigation menu toggle */
  $('a#full-menu-toggle').on('click',function(e) {
    e.preventDefault();
    $(this).toggleClass('menu-active');
    $('#dul-masthead-region-megamenu').slideToggle();
  });


  /* display barcodes */
  $('.barcode-content').each(function() {
    var theBarcode = $(this).parents(".item").data("item-barcode");
    if (theBarcode) {
      $(this).text(theBarcode);
    } else {
      $(this).text("no barcode");
    }
  });

  /* Toggle staff view: show barcodes, etc. */
  $(".staff-view-toggle").on("click",function(e) {
    e.preventDefault();
    var show_text = $(this).data('show-text');
    var hide_text = $(this).data('hide-text');
    $(this).toggleClass('shown');
    $(this).closest('.items').find('.barcode-wrapper').fadeToggle();
    if ( $(this).text() == show_text ) {
      $(this).text(hide_text);
    } else {
      $(this).text(show_text);
    }
  });


  /* position tooltips */
  $("#holdings ul.single-link a").tooltip({placement : 'right'});
  $("#holdings button.link-type-fulltext").tooltip({placement : 'right'});
  $("#documents ul.single-link a").tooltip({placement : 'right'});
  $("#documents button.link-type-fulltext").tooltip({placement : 'right'});

  /* enable tooltips */
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });



  /* check if a user is touching their device */
  window.addEventListener('touchstart', function onFirstTouch() {

    /* turn off tooltips */
    $('[data-toggle="tooltip"]').tooltip('dispose');

    /* turn off staff hover */
    $("#content .items .staff-view-toggle-wrapper").css({ "display":"block" });

    /* make location buttons larger */
    $("#holdings .items .location-modal-toggle").addClass('touch-enabled');
    $("#documents .items .location-modal-toggle").addClass('touch-enabled');


    window.removeEventListener('touchstart', onFirstTouch, false);
  }, false);

  /* When the Request button is clicked for a TRLN result, show */
  /* a modal with links to choose your home library. Add the    */
  /* corresponding ILLiad request parameters to those links.    */

  $(document).on("click", ".request-from-trln", function () {
    var $req_button = $(this);
    var illiad_params = $req_button.data('illiad-params');
    $( "#choose-your-illiad a" ).each(function(){
      new_href = ( $(this).data('base-url') + '?' + illiad_params );
      $(this).attr( "href", new_href );
      /* pass the doc ID on to the link for analytics */
      $(this).data('document-id',
        ($req_button.closest('[data-document-id]').data('document-id') ||
          $('#document').data('document-id')));
    });
  });



  /* UPDATE AVAILABILITY */
  /* uses request app API */

  /* if this is a show page */
  if ( $('body').hasClass('blacklight-catalog-show') ) {
    var document_id = $( "#document" ).data('document-id').replace(/\D/g,'');
    /* only run call if there is a current status */
    if ( $('.status-badge').length > 0 ) {
      get_the_availability(document_id);
    }
  };

  /* if this is a trln-context show page */
  if ( $('body').hasClass('blacklight-trln-show') ) {
    var raw_id = $( "#document" ).data('document-id');
    if ( raw_id.startsWith("DUKE") ) {
      var document_id = raw_id.replace(/\D/g,'');
      /* only run call if there is a current status */
      if ( $('.status-badge').length > 0 ) {
        get_the_availability(document_id);
      }
    }
  };

  /* if this is a results page */
  if ( $('body').hasClass('blacklight-catalog-index') ) {
    $('.document').each(function() {
      document_id = $( this ).data('document-id').replace(/\D/g,'');
      /* only run call if there is a current status */
      if ( $( this ).find('.status-badge').length > 0 ) {
        get_the_availability(document_id);
      }
    });
  };

  /* if this is a TRLN results page (only check Duke records) */
  if ( $('body').hasClass('blacklight-trln-index') ) {
    $('section.document').each(function() {
      //raw_id = $( this ).find( "h3.index_title a" ).attr('href');
      raw_id = $( this ).data('document-id');
      if ( raw_id.startsWith("DUKE") ) {
        document_id = raw_id.replace(/\D/g,'');
        /* only run call if there is a current status */
        if ( $( this ).find('.items .institution-availability .institution span.available').length > 0 ) {
          get_the_availability(document_id);
        }
      }
    });
  };

  function escape_id( the_id ) {
    return the_id.replace( /(:|\.|\[|\]|,|=|@|\/|\&|\$|\%|\+)/g, "\\$1" );
  }

  /* API call function */
  /* needs a document_id, makes api call, updates DOM */

  function get_the_availability(document_id) {

    // TODO this needs to NOT be hard-coded
    var endpoint = '';
    var elementIdKey = '';

    // Introducing an 'altElementKey' variable for the scenario
    // when an item is in transit and has a "temporary" location
    var altElementKey = '';

    // If 'blacklight-catalog-index' or 'blacklight-trln-index' is present in
    // the <BODY> element's class list, then only call 'circstatushold'
    //
    // For "show" pages, determine if the record is:
    // - "holding-availability"-aware, or
    // - "item-availability"-aware
    //
    // ... then set the appropriate endpoint.
    //
    // DevNote:
    // I would have preferred a "one-liner" call here, similar to Ruby's [list].include?('my-thing'), but...
    // -dlc32
    if ( $('body').hasClass('blacklight-catalog-index') ) {
        endpoint = '/circstatushold/';
        elementIdKey = '#holding-';
        altElementKey = '.tempLoc-';
    } else if ( $('body').hasClass('blacklight-trln-index') ) {
        endpoint = '/circstatushold/';
    } else {
        if ( $('.holding-status-badge').length > 0 ) {
          endpoint = '/circstatushold/';
          elementIdKey = '#holding-';
          altElementKey = '.tempLoc-';
        } else {
          endpoint = '/circstatusitm/';
          elementIdKey = '#item-';
        }
    }

    // Exit "stage left" when endpoint is blank.
    if (endpoint == '') {
      return;
    }

    var api_url = $('#footer').attr('data-request-app-url') + endpoint;

    // Let's use fetch API instead of xmlhttp
    fetch(api_url + document_id)
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok ' + response.statusText);
      }
      return response.json(); // Parse the JSON from the response
    })
    .then(availability_response => {

      let availStatusHash = {};

      // Check for presence of permLoc and/or tempLoc in the API response
      if ( (availability_response["permLoc"] && Object.keys(availability_response["permLoc"]).length !== 0) ||
           (availability_response["tempLoc"] && Object.keys(availability_response["tempLoc"]).length !== 0) ) {

        // merge locations with prioritization of 'Available.' status
        const mergeWithPriority = (locType) => {
          Object.entries(availability_response[locType]).forEach(([key, value]) => {
            // If the key already exists in the hash, prioritize 'Available.'
            if (availStatusHash[key]) {
              if (value.label === 'Available.' && availStatusHash[key].label !== 'Available.') {
                availStatusHash[key] = value;
              }
            } else {
              // Otherwise, just add the entry to the hash
              availStatusHash[key] = value;
            }
          });
        };

        // Process both permLoc and tempLoc
        if (availability_response["permLoc"]) {
          mergeWithPriority("permLoc");
        }
        if (availability_response["tempLoc"]) {
          mergeWithPriority("tempLoc");
        }

      } else {
        // Default to availability_response itself if neither permLoc nor tempLoc is present
        availStatusHash = availability_response;
      }

      // If the API response is empty, manually update the 'Check Holdings' status
      if ( availStatusHash.permLoc && Object.keys(availStatusHash.permLoc).length === 0 &&
           availStatusHash.tempLoc && Object.keys(availStatusHash.tempLoc).length === 0 ) {

        // catalog show page
        if ( $('body').hasClass('blacklight-catalog-show') ) {
          $('.status-wrapper').each(function() {
            var item_id = $(this).attr('id'); // Use the document_id as identifier
            if (item_id) {
              var id = item_id.replace('holding-', '');
              updateStatus(id, 'Check Holdings.', 'item-availability-misc', '', '');
            }
          });
        }

        // trln item page
        if ( $('body').hasClass('blacklight-trln-show') ) {
          $('h3.institution-name.duke').next('div').find('.status-wrapper').each(function() {
            var item_id = $(this).attr('id'); // Use the document_id as identifier
            if (item_id) {
              var id = item_id.replace('holding-', '');
              updateStatus(id, 'Check Holdings.', 'item-availability-misc', '', '', 'show');
            }
          });
        }

        // catalog search results
        if ( $('body').hasClass('blacklight-catalog-index') ) {
          var item_h3 = $('h3.document-title-heading[id*="DUKE' + document_id + '"]');
          var item_card = item_h3.closest('div.index-header-and-metadata').siblings('div.items.card');
          item_card.find('div.status-wrapper').each(function() {
            var item_id = $(this).attr('id'); // Use the document_id as identifier
            if (item_id) {
              var id = item_id.replace('holding-', '');
              updateStatus(id, 'Check Holdings.', 'item-availability-misc', '', '');
            }
          });
        }

        // trln search results
        if ( $('body').hasClass('blacklight-trln-index') ) {
          $('section[data-document-id="DUKE' + document_id + '"]').each(function() {
            var selector = $(this).find('h4:contains("Duke Libraries")');
            var id = selector.attr('id').replace('holding-', '');
            updateStatus(id, 'Check Holdings.', 'item-availability-misc', '', '', 'index');
          });
        }


      } else {

        if ( $('body').hasClass('blacklight-trln-index') ) {
          // trln index has funky markup
          $.each(availStatusHash, function(id, status) {
            updateStatus(id, status.label, status.available, status.libCode, status.collCode, 'index');
          });

        } else {
          // 'normal' updating
          $.each(availStatusHash, function(id, status) {
            updateStatus(id, status.label, status.available, status.libCode, status.collCode);
          });

        }

      }
    })
    .catch(error => {
      console.error('problem with fetch:', error);
    });

    // update the status text and class
    function updateStatus(selectorOrId, label, availability, libCode, collCode, trlnView = '') {
      var myParent, mySpan;

      if (trlnView == 'show') {
        mySpan = $('#holding-' + selectorOrId);
        myParent = mySpan.closest('h4');
      } else if (trlnView == 'index') {
        myParent = $('section[data-document-id="DUKE' + document_id + '"]')
        mySpan = $(`#holding-${selectorOrId} span`);
      } else {
        var s1 = elementIdKey + escape_id(selectorOrId);
        var s2 = altElementKey + libCode + '-' + collCode;
        var s3 = '[data-doc-id="' + document_id + '"]';
        var selector = s1 + s3 + ', ' + s2 + s3;

        if (/^\d/.test(selectorOrId)) { // if id starts with a number, check for duplicates (we can't match on the same ID)
          myParent = $(selector).filter(function(index) {
            // Only get the first instance
            return index === 0;
          });
          mySpan = $('dl dd span', myParent).first();
        } else { // these are generally temp locations from the API
          myParent = $(selector);
          mySpan = $('dl dd span', selector);
        }
      }

      // Replace 'Check Holdings.' with link to request (for duke records)
      function replaceCheckHoldings(mySpan, label) {

        if (label !== 'Check Holdings.') {
          return;
        }

        if ($('body').hasClass('blacklight-catalog-index') || $('body').hasClass('blacklight-trln-index')) {
          request_link = mySpan.closest('section').find('div.index-document-functions a').attr("href");
        } else {
          request_link = $("#show-sidebar-inner .request-from-duke").attr("href");
        }

        // Make sure to add extra 'available' class for trln (to fix bold text)
        var linkClasses = 'avalability-details';
        if ($('body').hasClass('blacklight-trln-show')) {
          linkClasses = 'available ' + linkClasses;
        }

        if (request_link != undefined) {
          mySpan.html('<a href="' + request_link + '" target="_blank" class="' + linkClasses + '">Availability Details<i class="fa fa-external-link align-middle" aria-hidden="true"></i></a>');
        }
      }

      if (mySpan.text() == "" || myParent.hasClass('status-updated')) {
        // skip for non-matching items OR already updated items
        return;
      } else {
        // Update the status text and animate it
        if (mySpan.text().trim() !== label.trim()) {
          myParent.addClass('status-updated');

          //** troubleshoot:
          // mySpan.text(mySpan.text() + ' | ' + label);
          // return

          mySpan.velocity("fadeOut", {
            duration: 500,
            complete: function() {
              mySpan.text(label);
            }
          }).velocity("fadeIn", {
            delay: 250,
            duration: 500,
            begin: function() {
              // Replace 'Check Holdings' if needed
              replaceCheckHoldings(mySpan, label);

              /* Update status class */
              switch (availability) {
                case 'yes':
                  mySpan.attr('class', $('body').hasClass('blacklight-trln-index') || $('body').hasClass('blacklight-trln-show') ? 'item-available available' : 'item-available');
                  break;
                case 'no':
                  mySpan.attr('class', $('body').hasClass('blacklight-trln-index') || $('body').hasClass('blacklight-trln-show') ? 'item-not-available available' : 'item-not-available');
                  break;
                case 'other':
                  mySpan.attr('class', $('body').hasClass('blacklight-trln-index') || $('body').hasClass('blacklight-trln-show') ? 'item-availability-misc available' : 'item-availability-misc');
                  break;
              }
            }
          });
        }
      }
    }
  }

});


Blacklight.onLoad(function() {

  // Show the bookmarks link if user has any
  checkBookmarkCount();

  /* Fix duplicate bookmark IDs on show pages (one in sidebar, */
  /* another in the mobile-only content view).                 */
  $("#show-sidebar").find("form").first().find("label").each(function(){
    original_id = $(this).attr("for");
    new_id = original_id + "_2";
    $(this).attr("for", new_id);
    $(this).find("#" + original_id).attr("id", new_id);
  });

  /* Update titles for sections in search results */

  if ( $("body").hasClass("blacklight-catalog-index") || $("body").hasClass("blacklight-trln-index") ||  $("body").hasClass("blacklight-bookmarks-index")) {

    $("*[id*=doctitleheader]").each(function() {

      // get item title
      var $theTitle = $("a", this).text();

      // get existing label
      var $theLabel = $(this).parent( "div" ).parent( "section").attr("aria-label");

      // append parent label
      $(this).parent( "div" ).parent( "section").attr("aria-label", $theLabel + ' -- ' + $theTitle);

    });

  }

  /* Add classes to the search results titles depending on whether they have */
  /* thumbnails, request buttons, both, or neither. Fade the request button */
  /* in once its final position is known, to prevent it from hopping after it */
  /* is initially rendered. */

  $(window).load(function(){

    $('.document').each(function() {
      var hasRequest = $(this).has("div.index-document-functions").length;
      var coverImage = $(this).children("div").find('img');

      if (0 in coverImage) {
        var hasWidth = coverImage.get(0).naturalWidth;
      } else {
        // Handle case where no Syndetics image is fetched due to lack
        // of necessary ID in the record. We hide the placeholder div
        // to prevent display issues.
        var hasWidth = 0;
        $(this).children(".thumbnail").addClass("hidden")
      }

      if (hasRequest && hasWidth > 2) {
        $(this).children(".documentHeader").addClass("has-request-and-thumbnail");
        $(this).children(".index-document-functions").css('opacity','1');
      }

      if (hasRequest && hasWidth < 2) {
        $(this).children(".documentHeader").addClass("has-request");
        $(this).children(".index-document-functions").css('opacity','1');
      }

      if (!hasRequest && hasWidth > 2) {
        $(this).children(".documentHeader").addClass("has-thumbnail");
      }

      if (!hasRequest && hasWidth < 2) {
        $(this).children(".documentHeader").addClass("no-request-nor-thumbnail");
      }

    });

  });

});
