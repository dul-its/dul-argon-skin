// This file generated from Blacklight 7.24.0 via TrlnArgon::Utilities.
// Changes may be overwritten!

// See documentation:
// https://github.com/trln/trln_argon/wiki/Blacklight-7---Argon-2-upgrade-notes#jsassets
// https://github.com/projectblacklight/blacklight/wiki/Using-Sprockets-to-compile-javascript-assets#customization-options

//= require 'blacklight/core.js'
//= require 'blacklight/search_context.js'
//= require 'blacklight/facet_load.js'
//= require 'blacklight/checkbox_submit.js'
//= require 'blacklight/button_focus.js'
//= require 'blacklight/modal.js'
//= require 'blacklight/bookmark_toggle.js'
