//DUL Override to fix a11y issue DS-1013
//File can be removed if/when this is fixed in TRLN Argon

// test if an element is in the viewport
$.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };
  
  Blacklight.onLoad(function() {
  
    $("#documents .hider, #holdings .hider").click(function(evt) {
      evt.preventDefault();
      var theAnchorID = $(this).attr('href');
      if (!$(theAnchorID).isInViewport()) {
        $('html,body').animate({scrollTop: $(theAnchorID).offset().top - 150}, 'fast');
      }
    });
  
    $("#document .less").click(function(evt) {
      evt.preventDefault();
      var theAnchorID = $(this).find(".btn").attr('href');
      if (!$(theAnchorID).isInViewport()) {
        $('html,body').animate({scrollTop: $(theAnchorID).offset().top - 50}, 'fast');
      }
    });
  
    var lineHeight = function(element) {
      var pxHeight = window.getComputedStyle(element, null).getPropertyValue("line-height");
      return parseInt(pxHeight.replace(/px$/, ''), 10);
    }
  
    //Override changes begin here
    $(".expandable-content").each( function() {
  
      var me = $(this);
      var parent = me.parent();
      var sibling = me.next();
      var expandControl = sibling.find('a');
      var origHeight = me.height();
      var contractedHeight = lineHeight(this) * 6 + 4;
  
      if ( origHeight <= contractedHeight ) {
        return;
      }
  
      var contract = function(evt) {
        me.addClass('contracted');
        if ( me.children(":first").is("ul") ) {
          me.addClass('contracted-ul');
        }
        expandControl.attr('aria-expanded', 'false');
        // switch buttons
        sibling.find('span.show-control.more').addClass( "d-inline" );
        sibling.find('span.show-control.less').removeClass( "d-inline" );
      }
  
      var expand = function(evt) {
        me.removeClass('contracted');
        me.removeClass('contracted-ul');
        expandControl.attr('aria-expanded', 'true');
        // switch buttons
        sibling.find('span.show-control.more').removeClass( "d-inline" );
        sibling.find('span.show-control.less').addClass( "d-inline" );;
      };
  
      contract(); // if we got this far, we want to shrink
  
      parent.find('.more').click(function() {
        expand();
      });
  
  
      parent.find('.less').click(function() {
        contract();
      });
  
    });

    //Override changes end here
  
  
  });
