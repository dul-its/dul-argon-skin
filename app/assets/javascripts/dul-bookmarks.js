// DUL CUSTOMIZATIONS on top of Blacklight's native bookmark behavior.

// Hide or show the mast bookmarks link conditionally depending on whether
// a user actually has any.
function checkBookmarkCount() {
  if ( $('#bookmarks_nav span[data-role="bookmark-counter"]').first().text() !== '0' ) {
    $('#bookmarks_nav').show();
  } else {
    $('#bookmarks_nav').hide();
  }
}

// Add tooltips to our custom-styled bookmark checkboxes
Blacklight.onLoad(function() {
  $('body').tooltip({ selector: '.checkbox.toggle-bookmark label',
                      title: function() {
                        return $(this).find('span').first().text() || 'add to folder'
                      },
                      placement: 'left'
                    });

  $('input.toggle-bookmark[type=checkbox]').on('click', function(){
    $(this).closest('label').tooltip('hide');
    setTimeout(function(){ checkBookmarkCount()}, 500);
  });
});
