# frozen_string_literal: true

# Overrides some methods from TRLN Argon's layout Helper
# which overrides BL core. See:
# https://github.com/trln/trln_argon/blob/master/app/helpers/layout_helper.rb
# https://github.com/projectblacklight/blacklight/blob/main/app/helpers/blacklight/layout_helper_behavior.rb
module LayoutHelper
  include Blacklight::LayoutHelperBehavior

  def main_content_classes
    'col-lg-9 col-md-8'
  end

  def sidebar_classes
    'page-sidebar col-lg-3 col-md-4'
  end
end
