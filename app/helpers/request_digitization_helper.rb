# frozen_string_literal: true

module RequestDigitizationHelper
  # rubocop:disable Metrics/CyclomaticComplexity
  def show_request_digitization_link?(document)
    DulArgonSkin.request_digitization_url.present? &&
      document.present? &&
      document.record_owner == 'duke' &&
      display_items?(document:) &&
      meets_resource_type_requirement?(document) &&
      meets_location_requirement?(document) &&
      meets_physical_media_requirement?(document) &&
      meets_publication_year_requirement?(document)
  end
  # rubocop:enable Metrics/CyclomaticComplexity

  def request_digitization_url(document)
    request_digitization_url_template.expand(
      'query' => request_digitization_params(document)
    ).to_s
  end

  private

  def request_digitization_params(document)
    { 'Title' => document.fetch(TrlnArgon::Fields::TITLE_MAIN,
                                'title unknown'),
      'Author' => document.names.first.to_h.fetch(:name, ''),
      'Published' => document.fetch(TrlnArgon::Fields::PUBLICATION_YEAR,
                                    'publication year unknown'),
      'SystemID' => strip_duke_id_prefix(document:) }
  end

  def request_digitization_url_template
    Addressable::Template.new(DulArgonSkin.request_digitization_url)
  end

  def meets_location_requirement?(document)
    loc_b_codes = document.holdings.keys
    loc_n_codes = document.holdings.map { |_, v| v.keys }.flatten

    DulArgonSkin.req_digi_loc_b_codes.intersect?(loc_b_codes) ||
      DulArgonSkin.req_digi_loc_n_codes.intersect?(loc_n_codes)
  end

  def meets_publication_year_requirement?(document)
    pub_year = document.fetch(TrlnArgon::Fields::PUBLICATION_YEAR, '0')
    pub_year.to_i < Time.now.year - 71 ||
      (pub_year.to_i < Time.now.year - 55 && published_in_usa?(document))
  end

  def published_in_usa?(document)
    document.fetch(TrlnArgon::Fields::ORIGIN_PLACE, [])
            .include?('United States')
  end

  def meets_physical_media_requirement?(document)
    physical_media_types = document.fetch(TrlnArgon::Fields::PHYSICAL_MEDIA, [])
    physical_media_types.include?('Print') &&
      physical_media_types.none? { |m| m.match?(/Microform/) }
  end

  def meets_resource_type_requirement?(document)
    resource_types = document.fetch(TrlnArgon::Fields::RESOURCE_TYPE, [])
    # Include Types
    resource_types.intersect?(['Book',
                               'Government publication',
                               'Music score',
                               'Thesis/Dissertation']) &&
      # Exclude Types
      (resource_types &
      ['Map',
       'Journal, Magazine, or Periodical']).none?
  end
end
