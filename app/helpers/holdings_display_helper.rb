# frozen_string_literal: true

module HoldingsDisplayHelper
  # With DUL's conversion to Alma, we need to
  # define helpers to get information about a title's
  # holding -- especially holdings availability
  def holding_availability_display(holding)
    case holding['status']
    when /^available/i
      'item-available'
    when /^unavailable/i
      'item-not-available'
    when /^check holdings/i
      'item-availability-misc'
    end
  end

  # NOTE: added by Derrek Croney - provide holding_id
  def get_holding_id(holding)
    holding.key?('holding_id') ? holding['holding_id'] : ''
  end

  # NOTE: added by Derrek Croney (duke) to capture the holding_id for a record
  def assign_holding_id_as_id(holding)
    holding.key?('holding_id') ? "id=\"holding-#{CGI.escapeHTML(holding['holding_id'])}\"".html_safe : ''
  end

  def temploc_attribute(item)
    if item.nil? || item.empty?
      ''
    else
      "tempLoc-#{item['loc_b']}-#{item['loc_n']}"
    end
  end

  def dt_folder_label?(item)
    item_types = %w[ARCHIVAL_MATERIAL MSS MANUSCRIPT]

    return true if item_types.include?(item['type'])

    false
  end
end
