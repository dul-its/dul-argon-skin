# frozen_string_literal: true

module ApplicationHelper
  def git_branch_info
    `git rev-parse --abbrev-ref HEAD`
  end

  # When running in K8S, the .git folder may not be present, or
  # it may be present but with a different owner (dubious ownership)
  #
  # So, let's have this function attempt to fetch an environment variable,
  # GIT_COMMIT_HASH and use it (or default to 'git rev-parse --short HEAD'
  def git_commit_hash
    rev_parse_hash = `git rev-parse --short HEAD`
    ENV.fetch('GIT_COMMIT_HASH', rev_parse_hash)
  end

  def git_commit_msg
    `git log --format=%B -n 1`.truncate(80)
  end

  # rubocop:disable Style/SafeNavigation
  def search_session_includes_ajax_request?(current_search_session = nil)
    current_search_session &&
      current_search_session.respond_to?(:query_params) &&
      current_search_session.query_params.fetch('format', nil).present? &&
      %w[json xml].include?(current_search_session.query_params
                                                  .fetch('format', ''))
  end

  def search_session_includes_catalog_controller?(current_search_session = nil)
    current_search_session &&
      current_search_session.respond_to?(:query_params) &&
      %w[catalog trln].include?(current_search_session.query_params
                                                      .fetch(:controller, ''))
  end

  # Hathitrust links can be displayed if the record:
  # 1) Has an OCLC Number
  # 2) Is NOT a Rubenstein record
  # 3) (Is NOT a Serial AND it was published before 1924) OR (Is a Gov Doc)
  #
  # 3) Is NOT a Serial OR
  #      it is a Gov Doc OR
  #      it was published before 1924
  def show_hathitrust_link_if_available?(document)
    document.oclc_number.present? &&
      !document.rubenstein_record? &&
      (
        (
          !document.fetch(TrlnArgon::Fields::RESOURCE_TYPE, []).include?('Journal, Magazine, or Periodical') &&
          document.fetch(TrlnArgon::Fields::PUBLICATION_YEAR, '9999').to_i < 1924
        ) ||
        document.fetch(TrlnArgon::Fields::RESOURCE_TYPE, [])
                .include?('Government publication')
      )
  end
  # rubocop:enable Style/SafeNavigation

  def strip_duke_id_prefix(options = {})
    doc = options.fetch(:document, nil)
    return unless doc

    doc.fetch(TrlnArgon::Fields::LOCAL_ID, '').sub('DUKE', '')
  end

  def add_bookplate_span_then_clean_and_format_links(options = {})
    bookplate_options = add_bookplate_span(options)
    clean_and_format_links(value: [bookplate_options])
  end

  def add_bookplate_span(options = {})
    options.fetch(:value, []).map do |v|
      if v.match(bookplate_regex)
        content_tag(:span, v, class: 'dul-bookplate')
      else
        v
      end
    end.join('<br/>').html_safe
  end

  def add_donor_span(options = {})
    options.fetch(:value, []).map do |v|
      content_tag(:span, v, class: 'dul-bookplate')
    end.join('<br/>').html_safe
  end

  def split_isbn_values(options = {})
    doc = options.fetch(:document, nil)
    return unless doc

    doc.fetch(TrlnArgon::Fields::ISBN_NUMBER, '')
  end

  private

  def bookplate_regex
    /^(in\smemory\sof\s|
       gift\sof\s|
       in\shonor\sof\s|
       the\sconservation\sof\s|
       in\sremembrance\sof\s|
       in\srecognition\sof\s|
       dul\sexceptional\sstaff\smember\s|
       duke\suniversity\slibraries\shonors\s)/xi
  end
end
