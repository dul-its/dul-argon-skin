# frozen_string_literal: true

# Overrides (really just replicates) file from TRLN Argon. See:
# https://github.com/trln/trln_argon/blob/master/app/components/trln_argon/search_bar_component.rb

module TrlnArgon
  class SearchBarComponent < Blacklight::SearchBarComponent
  end
end
