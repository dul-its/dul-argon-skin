# frozen_string_literal: true

class Setting < ApplicationRecord
  def self.etas_enabled?
    false
    # Rails.cache.fetch('hathitrust_etas_enabled', expires_in: 1.minute) do
    #  ActiveModel::Type::Boolean.new.cast(
    #    Setting.where(name: 'etas_enabled').first&.value || false
    #  )
    # end
  end
end
