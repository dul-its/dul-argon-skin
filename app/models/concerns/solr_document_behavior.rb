# frozen_string_literal: true

module SolrDocumentBehavior
  extend ActiveSupport::Concern

  # rubocop:disable Metrics
  included do
    send(:include, Blacklight::Solr::Document)
    # This is needed so that find will work correctly
    # from the Rails console with our Solr configuration.
    # Otherwise, it tries to use the non-existent document request handler.
    repository.blacklight_config.document_solr_path = :document
    repository.blacklight_config.document_solr_request_handler = nil
    use_extension(TrlnArgon::DocumentExtensions::Ris)
    use_extension(TrlnArgon::DocumentExtensions::Email)
    use_extension(TrlnArgon::DocumentExtensions::Sms)
    use_extension(Blacklight::Document::DublinCore)

    send(:include, TrlnArgon::SolrDocument)
    send(:include, TrlnArgon::ItemDeserializer)
    send(:include, RequestItem)
    send(:include, SmsFieldMapping)
    send(:include, Syndetics)

    def urls
      @urls ||= deserialized_urls.each do |url|
        url[:href] = fix_old_proxy(url[:href])
      end
    end

    def fix_old_proxy(url)
      url.gsub('//proxy.lib.duke.edu/', '//login.proxy.lib.duke.edu/')
    end

    # Overrides TRLN Argon method from
    # trln/trln_argon/blob/main/lib/trln_argon/item_deserializer.rb
    def holdings
      return unless self[TrlnArgon::Fields::INSTITUTION]

      institution = self[TrlnArgon::Fields::INSTITUTION].first

      @holdings ||= if institution == 'duke' && mms_id.present?
                      deserialize_duke_holdings
                    else
                      deserialize_holdings
                    end
      @holdings
    end

    def mms_id
      return unless self[TrlnArgon::Fields::MMS_ID]

      @mms_id ||= self[TrlnArgon::Fields::MMS_ID].first
      @mms_id
    end

    # Overrides TRLN Argon method from
    # trln/trln_argon/blob/main/lib/trln_argon/item_deserializer.rb

    def deserialize_duke_holdings
      items = (self[TrlnArgon::Fields::ITEMS] || {}).map { |x| JSON.parse(x) }
      holdings = (self[TrlnArgon::Fields::HOLDINGS] || {}).map { |x| JSON.parse(x) }

      # Group items by their location (loc_b and loc_n)
      items_intermediate = items.group_by { |i| [i['loc_b'], i['loc_n']] }

      h_final = items_intermediate.each_with_object({}) do |((loc_b, loc_n), loc_items), acc|
        acc[loc_b] ||= {}
        acc[loc_b][loc_n] = {
          'items' => loc_items,
          'holdings' => select_matching_holdings(holdings, loc_b, loc_n)
        }

        # Ensure that holdings exist even if they were not matched
        if acc[loc_b][loc_n]['holdings'].empty?
          acc[loc_b][loc_n]['holdings'] << { 'summary' => '', 'call_no' => cn_prefix(loc_items) }
        end
      end

      # Add any remaining holdings that were not matched by loc_b and loc_n
      holdings.each do |holding|
        loc_b = holding['loc_b']
        loc_n = holding['loc_n']
        loc_map = h_final[loc_b] ||= {}
        loc_map[loc_n] ||= { 'items' => [], 'holdings' => [] }
        loc_map[loc_n]['holdings'] << holding if loc_map[loc_n]['holdings'].empty?
      end

      h_final.reject { |k, _v| k.nil? }
    end

    def select_matching_holdings(holdings, loc_b, loc_n)
      holdings.select { |h| h['loc_b'] == loc_b && h['loc_n'] == loc_n }
    end

    def cn_prefix(items)
      return '' unless items.any? { |item| item['call_no'].present? }

      item_with_call_no = items.find { |item| item['call_no'].present? }
      item_with_call_no['call_no'].split.first
    end
  end
  # rubocop:enable Metrics

  # Per Alma migration, "Rubenstein Record" is true when
  # a records's holdings include SCL or ARCH
  def rubenstein_record?
    holdings.key?('SCL') or holdings.key?('ARCH')
  end

  # Per Alma migration, "LSC Record" is true when
  # a records's holdings include LSC
  def lsc_record?
    holdings.key?('LSC')
  end
end
