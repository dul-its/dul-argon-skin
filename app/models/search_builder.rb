# frozen_string_literal: true

class SearchBuilder < Blacklight::SearchBuilder
  include Blacklight::Solr::SearchBuilderBehavior
  include BlacklightRangeLimit::RangeLimitBuilder

  include BlacklightAdvancedSearch::AdvancedSearchBuilder
  include TrlnArgon::ArgonSearchBuilder
  include ArgonCallNumberSearch::SearchBuilderBehavior

  self.default_processor_chain += %i[add_advanced_search_to_solr
                                     add_call_number_query_to_solr
                                     min_match_for_initial_articles]

  # Override method from TRLN Argon to remove spellcheck param
  def remove_params_for_count_only_query(solr_parameters)
    solr_parameters.delete('stats')
    solr_parameters.delete('stats.field')
    solr_parameters.delete('expand')
    solr_parameters.delete('expand.rows')
    solr_parameters.delete('expand.q')
    solr_parameters.delete('spellcheck')
  end

  # Adjust min match setting for title searches with inital articles
  # Users should be able to search for titles with or without
  # initial articles even if the title does not include them.
  def min_match_for_initial_articles(solr_parameters)
    return unless includes_title_search?

    query = blacklight_params.fetch(:q, nil) ||
            blacklight_params.fetch('title', nil)

    return unless query.present? && query.strip.match?(/^(the|a|an)\s/i)

    token_count = query.gsub(/[[:punct:]]/, ' ').split.count

    return unless token_count > 3

    solr_parameters[:mm] = "#{token_count - 1}<95%"
  end

  def includes_title_search?
    blacklight_params.key?('search_field') &&
      ((blacklight_params['search_field'] == 'title') ||
      (blacklight_params['search_field'] == 'advanced' &&
      blacklight_params.fetch('title', nil).present?))
  end

  ##
  # @example Adding a new step to the processor chain
  #   self.default_processor_chain += [:add_custom_data_to_query]
  #
  #   def add_custom_data_to_query(solr_parameters)
  #     solr_parameters[:custom] = blacklight_params[:user_value]
  #   end
end
