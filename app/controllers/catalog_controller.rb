# frozen_string_literal: true

# rubocop:disable Metrics/ClassLength
class CatalogController < ApplicationController
  include BlacklightRangeLimit::ControllerOverride
  include Blacklight::Catalog

  # CatalogController behavior and configuration for TrlnArgon
  include TrlnArgon::ControllerOverride
  prepend CitationAction

  def request_is_json?
    request.format.json?
  end

  rescue_from Blacklight::Exceptions::RecordNotFound do
    redirect_to '/404'
  end

  before_action do
    if request.format.json?
      blacklight_config.add_index_field TrlnArgon::Fields::ISBN_NUMBER_ISBN.to_s,
                                        label: TrlnArgon::Fields::ISBN_NUMBER_ISBN.label
    end

    # Add some Duke-specific tools, but omit them from the bookmarks page
    unless controller_name == 'bookmarks'
      blacklight_config.add_results_collection_tool(:rss_button,
                                                    partial: 'rss_button')
      blacklight_config.add_show_tools_partial(:report_missing,
                                               partial: 'report_missing_item')
    end
  end

  # DUL Customization: make Advanced Search facet fields NOT limited to those
  # configured as home_facets. Local fix for https://trlnmain.atlassian.net/browse/TD-1263.
  # See:
  # https://github.com/projectblacklight/blacklight/blob/release-7.x/app/controllers/concerns/blacklight/catalog.rb#L65-L67
  def advanced_search
    (@response, _deprecated_document_list) = blacklight_advanced_search_form_search_service.search_results do |builder|
      builder.except(:only_home_facets)
    end
  end

  configure_blacklight do |config|
    config.raw_endpoint.enabled = true

    # DUL CUSTOMIZATION: added show_harmful_language_statement to item show
    config.show.partials = %i[show_items
                              show_authors
                              show_summary
                              show_included_works
                              show_subjects
                              show
                              show_related_works
                              show_harmful_language_statement]

    # Switched index partial order so text can wrap around the thumbnail
    # NOTE: index_document_actions_default is a custom DUL partial
    config.index.partials =
      %i[thumbnail index_document_actions index_header index_items]

    # Add Request button using Blacklight's extensible "tools" for
    # index view
    # Note: delete bookmark then add back for proper ordering
    config.index.document_actions.delete(:bookmark)

    config.add_results_document_tool(:request_button, partial: 'request_button')

    config.add_results_document_tool(:bookmark,
                                     partial: 'bookmark_control',
                                     if: :render_bookmarks_control?)

    config.add_results_collection_tool(:sort_widget)
    config.add_results_collection_tool(:per_page_widget)
    config.add_results_collection_tool(:view_type_group)

    # Configure tools for "Send Info To..." dropdown on item page
    config.add_show_tools_partial(:email,
                                  icon: 'fa fa-envelope',
                                  callback: :email_action,
                                  path: :email_path,
                                  validator: :validate_email_params)
    config.add_show_tools_partial(:sms,
                                  icon: 'fa fa-commenting',
                                  if: :render_sms_action?,
                                  callback: :sms_action,
                                  path: :sms_path,
                                  validator: :validate_sms_params)
    config.add_show_tools_partial(:citation,
                                  icon: 'fa fa-quote-left',
                                  if: :render_citation_action?)
    config.add_show_tools_partial(:ris,
                                  icon: 'fa fa-download',
                                  if: :render_ris_action?,
                                  modal: false,
                                  path: :ris_path)
    config.add_show_tools_partial(:refworks,
                                  icon: 'fa fa-list',
                                  if: :render_refworks_action?,
                                  new_window: true,
                                  modal: false,
                                  path: :refworks_path)
    config.add_show_tools_partial(:bookmark,
                                  partial: 'bookmark_control',
                                  if: :render_bookmarks_control?)

    # Remove Bookmark button from show view dropdown
    config.show.document_actions.delete(:bookmark)

    config.add_nav_action(:bookmark,
                          partial: 'blacklight/nav/bookmark',
                          if: :render_bookmarks_control?)
    config.add_nav_action(:search_history,
                          partial: 'blacklight/nav/search_history')

    ## Class for sending and receiving requests from a search index
    # config.repository_class = Blacklight::Solr::Repository
    #
    ## Class for converting Blacklight's url parameters to into request params
    #  for the search index config.search_builder_class = ::SearchBuilder
    #
    ## Model that maps search index responses to the blacklight response model
    # config.response_model = Blacklight::Solr::Response

    ## Default parameters to send to solr for all search-like requests.
    #  See also SearchBuilder#processed_parameters
    config.default_solr_params = {
      rows: 10,
      lowercaseOperators: 'false', # https://issues.apache.org/jira/browse/SOLR-4646
      spellcheck: 'true'
    }

    config.advanced_search[:form_solr_parameters] = {
      # NOTE: You will not get any facets back
      #       on the advanced search page
      #       unless defType is set to lucene.
      'defType' => 'lucene',
      'facet.field' => [TrlnArgon::Fields::AVAILABLE_FACET.to_s,
                        TrlnArgon::Fields::ACCESS_TYPE_FACET.to_s,
                        TrlnArgon::Fields::RESOURCE_TYPE_FACET.to_s,
                        TrlnArgon::Fields::PHYSICAL_MEDIA_FACET.to_s,
                        TrlnArgon::Fields::LANGUAGE_FACET.to_s,
                        TrlnArgon::Fields::CALL_NUMBER_FACET.to_s,
                        TrlnArgon::Fields::LOCATION_HIERARCHY_FACET.to_s],
      'f.resource_type_f.facet.limit' => -1, # return all resource type values
      'f.language_f.facet.limit' => -1, # return all language facet values
      'f.call_number_f.facet.limit' => -1, # return all call number values
      'f.location_hierarchy_f.facet.limit' => -1, # return all loc facet values
      'f.physical_media_f.facet.limit' => -1, # return all phys med facet values
      'facet.limit' => -1, # return all facet values
      'facet.sort' => 'index' # sort by byte order of values
    }

    # solr path which will be added to solr base url before other solr params
    # config.solr_path = 'select'

    # items to show per page, each number in the array represent another
    # option to choose from.
    # config.per_page = [10,20,50,100]

    ## Default parameters to send on single-document requests to Solr.
    # These settings are the BL defaults (see SearchHelper#solr_doc_params) or
    ## parameters included in the Blacklight-jetty document requestHandler.
    #
    # config.default_document_solr_params = {
    #  qt: 'document',
    #  ## These are hard-coded in the blacklight 'document' requestHandler
    #  # fl: '*',
    #  # rows: 1,
    #  # q: '{!term f=id v=$id}'
    # }

    # solr field configuration for search results/index views
    # config.index.thumbnail_field = 'thumbnail_path_ss'

    # solr field configuration for document/show views
    # config.show.title_field = 'title_display'
    # config.show.display_type_field = 'format'
    # config.show.thumbnail_field = 'thumbnail_path_ss'

    config.add_index_field TrlnArgon::Fields::TITLE_MAIN.to_s,
                           label: TrlnArgon::Fields::TITLE_MAIN.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::ISBN_NUMBER.to_s,
                           if: :request_is_json?,
                           helper_method: :split_isbn_values
    config.add_index_field TrlnArgon::Fields::OCLC_NUMBER.to_s,
                           label: TrlnArgon::Fields::OCLC_NUMBER.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::UPC.to_s,
                           label: TrlnArgon::Fields::UPC.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::NOTE_SUMMARY.to_s,
                           label: TrlnArgon::Fields::NOTE_SUMMARY.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::ACCESS_TYPE.to_s,
                           label: TrlnArgon::Fields::ACCESS_TYPE.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::AVAILABLE.to_s,
                           label: TrlnArgon::Fields::AVAILABLE.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::CALL_NUMBER_SCHEMES.to_s,
                           label: TrlnArgon::Fields::CALL_NUMBER_SCHEMES.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::DATE_CATALOGED.to_s,
                           label: TrlnArgon::Fields::DATE_CATALOGED.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::DONOR.to_s,
                           label: TrlnArgon::Fields::DONOR.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::GENRE_HEADINGS.to_s,
                           label: TrlnArgon::Fields::GENRE_HEADINGS.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::HOLDINGS.to_s,
                           label: TrlnArgon::Fields::HOLDINGS.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::IMPRINT_MULTIPLE.to_s,
                           label: TrlnArgon::Fields::IMPRINT_MULTIPLE.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::INSTITUTION.to_s,
                           label: TrlnArgon::Fields::INSTITUTION.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::ISBN_QUALIFYING_INFO.to_s,
                           label: TrlnArgon::Fields::ISBN_QUALIFYING_INFO.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::ITEMS.to_s,
                           label: TrlnArgon::Fields::ITEMS.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::LANGUAGE.to_s,
                           label: TrlnArgon::Fields::LANGUAGE.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::LC_CALL_NOS_NORMED.to_s,
                           label: TrlnArgon::Fields::LC_CALL_NOS_NORMED.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::LCC_CALLNUM_CLASSIFICATION.to_s,
                           label: TrlnArgon::Fields::LCC_CALLNUM_CLASSIFICATION.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::LCC_TOP.to_s,
                           label: TrlnArgon::Fields::LCC_TOP.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::LOCAL_ID.to_s,
                           label: TrlnArgon::Fields::LOCAL_ID.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::MISC_ID.to_s,
                           label: TrlnArgon::Fields::MISC_ID.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::MMS_ID.to_s,
                           label: TrlnArgon::Fields::MMS_ID.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::NAMES.to_s,
                           label: TrlnArgon::Fields::NAMES.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::NOTE_GENERAL.to_s,
                           label: TrlnArgon::Fields::NOTE_GENERAL.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::NOTE_TOC.to_s,
                           label: TrlnArgon::Fields::NOTE_TOC.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::ORIGIN_PLACE.to_s,
                           label: TrlnArgon::Fields::ORIGIN_PLACE.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::OWNER.to_s,
                           label: TrlnArgon::Fields::OWNER.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::PHYSICAL_DESCRIPTION.to_s,
                           label: TrlnArgon::Fields::PHYSICAL_DESCRIPTION.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::PRIMARY_ISBN.to_s,
                           label: TrlnArgon::Fields::PRIMARY_ISBN.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::PRIMARY_OCLC.to_s,
                           label: TrlnArgon::Fields::PRIMARY_OCLC.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::PUBLICATION_YEAR.to_s,
                           label: TrlnArgon::Fields::PUBLICATION_YEAR.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::PUBLISHER_LOCATION.to_s,
                           label: TrlnArgon::Fields::PUBLISHER_LOCATION.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::ROLLUP_ID.to_s,
                           label: TrlnArgon::Fields::ROLLUP_ID.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::SUBJECT_GENRE.to_s,
                           label: TrlnArgon::Fields::SUBJECT_GENRE.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::SHELFKEY.to_s,
                           label: TrlnArgon::Fields::SHELFKEY.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::SUBJECT_HEADINGS.to_s,
                           label: TrlnArgon::Fields::SUBJECT_HEADINGS.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::SUBJECT_TOPICAL.to_s,
                           label: TrlnArgon::Fields::SUBJECT_TOPICAL.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::THIS_WORK.to_s,
                           label: TrlnArgon::Fields::THIS_WORK.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::TITLE_SORT.to_s,
                           label: TrlnArgon::Fields::TITLE_SORT.label,
                           if: :request_is_json?
    config.add_index_field TrlnArgon::Fields::URLS.to_s,
                           label: TrlnArgon::Fields::URLS.label,
                           if: :request_is_json?

    config.home_facet_fields.delete(TrlnArgon::Fields::AVAILABLE_FACET)
    config.facet_fields.delete(TrlnArgon::Fields::AVAILABLE_FACET)

    # Temporarily disable slow facets on home page until caching is
    # enabled in TRLN Argon.
    config.home_facet_fields.delete(TrlnArgon::Fields::CALL_NUMBER_FACET)
    config.home_facet_fields.delete(TrlnArgon::Fields::LANGUAGE_FACET)

    # Delete but save date cataloged facet so we can add it again
    # in the last position.
    date_cataloged = config.home_facet_fields.delete(
      TrlnArgon::Fields::DATE_CATALOGED_FACET
    )
    config.add_home_facet_field TrlnArgon::Fields::PHYSICAL_MEDIA_FACET.to_s,
                                label: TrlnArgon::Fields::
                                       PHYSICAL_MEDIA_FACET.label,
                                limit: true,
                                collapse: false
    config.add_home_facet_field date_cataloged

    # solr fields that will be treated as facets by the blacklight application
    #   The ordering of the field names is the order of the display
    #
    # Setting a limit will trigger Blacklight's 'more' facet values link.
    # * If left unset, then all facet values returned by solr will be displayed.
    # * If set to an integer, then "f.somefield.facet.limit" will be added to
    # solr request, with actual solr request being +1 your configured limit --
    # you configure the number of items you actually want _displayed_ in a page.
    # * If set to 'true', then no additional parameters will be sent to solr,
    # but any 'sniffed' request limit parameters will be used for paging, with
    # paging at requested limit -1. Can sniff from facet.limit or
    # f.specific_field.facet.limit solr request params. This 'true' config
    # can be used if you set limits in :default_solr_params, or as defaults
    # on the solr side in the request handler itself. Request handler defaults
    # sniffing requires solr requests to be made with "echoParams=all", for
    # app code to actually have it echo'd back to see it.
    #
    # :show may be set to false if you don't want the facet to be drawn in the
    # facet bar
    #
    # set :index_range to true if you want the facet pagination view to have
    # facet prefix-based navigation
    #  (useful when user clicks "more" on a large facet and wants to navigate
    # alphabetically across a large set of results)
    # :index_range can be an array or range of prefixes that will be used to
    # create the navigation (note: It is case sensitive when searching values)

    # Have BL send all facet field names to Solr, which has been the default
    # previously. Simply remove these lines if you'd rather use Solr request
    # handler defaults, or have no facets.
    config.add_facet_fields_to_solr_request!

    # solr fields to be displayed in the index (search results) view
    #   The ordering of the field names is the order of the display
    # solr fields to be displayed in the show (single result) view
    #   The ordering of the field names is the order of the display
    # "fielded" search configuration. Used by pulldown among other places.
    # For supported keys in hash, see rdoc for Blacklight::SearchFields
    #
    # Search fields will inherit the :qt solr request handler from
    # config[:default_solr_parameters], OR can specify a different one
    # with a :qt key/value. Below examples inherit, except for subject
    # that specifies the same :qt as default for our own internal
    # testing purposes.
    #
    # The :key is what will be used to identify this BL search field internally,
    # as well as in URLs -- so changing it after deployment may break bookmarked
    # urls.  A display label will be automatically calculated from the :key,
    # or can be specified manually to be different.

    # This one uses all the defaults set by the solr request handler. Which
    # solr request handler? Set in config[:default_solr_parameters][:qt],
    # since we aren't specifying it otherwise.

    # Now we see how to over-ride Solr request handler defaults, in this
    # case for a BL "search field", which is really a dismax aggregate
    # of Solr search fields.

    config.add_search_field('call_number') do |field|
      field.label = I18n.t('trln_argon.search_fields.call_number')
      field.advanced_parse = false
      field.include_in_advanced_search = false
    end

    # Delete but save isbn_issn search_field config
    # so we can add it again in the last position.
    isbn_issn = config.search_fields.delete('isbn_issn')

    # Delete but save series_statement search_field config
    # so we can add it again after publisher.
    series_statement = config.search_fields.delete('series_statement')

    # rubocop:disable Layout/HashAlignment
    config.add_search_field('origin_place') do |field|
      field.include_in_simple_select = false
      field.label = I18n.t('trln_argon.search_fields.origin_place')
      field.def_type = 'edismax'
      field.solr_local_parameters = {
        qf:  '$origin_place_qf',
        pf:  '$origin_place_pf',
        pf3: '$origin_place_pf3',
        pf2: '$origin_place_pf2'
      }
    end
    # rubocop:enable Layout/HashAlignment

    config.add_search_field(series_statement)

    config.add_search_field(isbn_issn)

    config.add_show_field TrlnArgon::Fields::LOCAL_ID.to_s,
                          label: TrlnArgon::Fields::LOCAL_ID.label,
                          helper_method: :strip_duke_id_prefix
    config.add_show_field TrlnArgon::Fields::DONOR.to_s,
                          label: TrlnArgon::Fields::DONOR.label,
                          helper_method: :add_donor_span
    config.add_show_field TrlnArgon::Fields::MMS_ID.to_s,
                          label: TrlnArgon::Fields::MMS_ID.label,
                          if: :request_is_json?

    config.show_fields[TrlnArgon::Fields::NOTE_LOCAL]
          .helper_method = :add_bookplate_span_then_clean_and_format_links

    # Specifying a :qt only to show it's possible, and so our internal automated
    # tests can test it. In this case it's the same as
    # config[:default_solr_parameters][:qt], so isn't actually neccesary.

    # "sort results by" select (pulldown)
    # label in pulldown is followed by the name of the SOLR field to sort by and
    # whether the sort is ascending or descending (it must be asc or desc
    # except in the relevancy case).

    config.add_sort_field "#{TrlnArgon::Fields::DATE_CATALOGED} desc, " \
                          "#{TrlnArgon::Fields::PUBLICATION_YEAR_SORT} desc",
                          label:
                          I18n.t('trln_argon.sort_options.date_cataloged_desc')

    # If there are more than this many search results, no spelling ("did you
    # mean") suggestion is offered.
    config.spell_max = 5

    # Configuration for autocomplete suggestor
    # config.autocomplete_enabled = false
    # config.autocomplete_path = 'suggest'
  end
end
# rubocop:enable Metrics/ClassLength
