# frozen_string_literal: true

module CitationAction
  extend ActiveSupport::Concern

  private

  # Override of trln_argon method to exclude certain Rubenstein
  # records from displaying the generate citation option.
  def render_citation_action?(_config, _options = {})
    docs = [@document || (@document_list || [])].flatten
    citation_action_configured? &&
      includes_docs_with_oclc_numbers?(docs) &&
      includes_non_archival_material?(docs)
  end

  def citation_action_configured?
    TrlnArgon::Engine.configuration.citation_formats.present?
  end

  def includes_docs_with_oclc_numbers?(docs)
    docs.any? { |doc| doc.oclc_number.present? }
  end

  def includes_non_archival_material?(docs)
    docs.delete_if { |doc| archival_material?(doc) }.any?
  end

  def archival_material?(doc)
    resource_types = doc.fetch(TrlnArgon::Fields::RESOURCE_TYPE, [])
    resource_types.include?('Archival and manuscript material') &&
      !resource_types.include?('Book') &&
      !resource_types.include?('Thesis/Dissertation')
  end
end
