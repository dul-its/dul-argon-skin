# frozen_string_literal: true

module DulArgonSkin
  autoload :Configurable, 'dul_argon_skin/configurable'
  include DulArgonSkin::Configurable
end
