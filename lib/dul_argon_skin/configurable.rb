# frozen_string_literal: true

module DulArgonSkin
  module Configurable
    extend ActiveSupport::Concern
    # rubocop:disable Metrics/BlockLength
    included do
      mattr_accessor :dul_home_url do
        ENV.fetch('DUL_HOME_URL', 'https://library.duke.edu')
      end

      # If present this string will display in a banner alert
      # at the top of each page to indicate which environment
      # the application is running in. Banner alert NOT shown if blank.
      mattr_accessor :environment_alert do
        ENV.fetch('ENVIRONMENT_ALERT', '')
      end

      # If present, messaging will display alerting users of Duke's
      # ALMA migration process and potential stale records
      mattr_accessor :alma_integration_alert do
        ENV.fetch('ALMA_INTEGRATION_ALERT', '')
      end

      # If 'true', output the data being sent to Matomo in
      # the browser console.
      mattr_accessor :matomo_debug do
        ENV.fetch('MATOMO_DEBUG', false)
      end

      # If tracking ID is present, activate Matomo tracking
      # and use that ID.
      mattr_accessor :matomo_tracking_id do
        ENV.fetch('MATOMO_TRACKING_ID', '')
      end

      # ILLiad base URLs for DUL & professional school libraries.
      # =========================================================
      mattr_accessor :illiad_dul_base_url do
        ENV.fetch('ILLIAD_DUL_BASE_URL', 'https://duke-illiad-oclc-org.proxy.lib.duke.edu/illiad/NDD/illiad.dll')
      end

      mattr_accessor :illiad_law_base_url do
        ENV.fetch('ILLIAD_LAW_BASE_URL', 'https://duke-illiad-oclc-org.proxy.lib.duke.edu/illiad/NDL/illiad.dll')
      end

      mattr_accessor :illiad_ford_base_url do
        ENV.fetch('ILLIAD_FORD_BASE_URL', 'https://duke-illiad-oclc-org.proxy.lib.duke.edu/illiad/NDB/illiad.dll')
      end

      mattr_accessor :illiad_med_base_url do
        ENV.fetch('ILLIAD_MED_BASE_URL', 'https://illiad.mclibrary.duke.edu/illiad.dll')
      end

      mattr_accessor :map_location_service_url do
        ENV.fetch('MAP_LOCATION_SERVICE_URL', 'https://library.duke.edu/locguide/mapinfo')
      end

      # Broad and narrow location codes that indicate online items.
      # =========================================================
      mattr_accessor :online_loc_b_codes do
        ENV.fetch('ONLINE_LOC_B_CODES', 'ONLINE, DUKIR').split(', ')
      end

      mattr_accessor :online_loc_n_codes do
        ENV.fetch('ONLINE_LOC_N_CODES', 'FRDE, database, LINRE, PEI, PENTL, MELEC').split(', ')
      end

      # Report missing item URL template
      mattr_accessor :report_missing_item_url do
        ENV.fetch('REPORT_MISSING_ITEM_URL', 'https://duke.qualtrics.com/jfe/form/SV_71J91hwAk1B5YkR/{?query*}')
      end

      # Request System base URL.
      mattr_accessor :request_base_url do
        ENV.fetch('REQUEST_BASE_URL', 'https://requests.library.duke.edu')
      end

      # Digitization request configurations
      # =========================================================
      mattr_accessor :request_digitization_url do
        ENV.fetch('REQUEST_DIGITIZATION_URL', 'https://duke.qualtrics.com/jfe/form/SV_9MOS64T5GlJ5bY9{?query*}')
      end

      mattr_accessor :req_digi_loc_b_codes do
        ENV.fetch('REQ_DIGI_LOC_B_CODES', 'PERKN, LILLY, MARIN, MUSIC, DOCS').split(', ')
      end

      mattr_accessor :req_digi_loc_n_codes do
        ENV.fetch('REQ_DIGI_LOC_N_CODES', 'PSB, PSL, PSK, PSM').split(', ')
      end

      # Search tips URL.
      mattr_accessor :search_tips_url do
        ENV.fetch('SEARCH_TIPS_URL', 'https://library.duke.edu/using/catalog-search-tips')
      end

      # About URL.
      mattr_accessor :about_url do
        ENV.fetch('ABOUT_URL', 'https://blogs.library.duke.edu/bitstreams/2019/01/07/new-duke-libraries-catalog-to-go-live-january-16/')
      end
    end
    # rubocop:enable Metrics/BlockLength
    module ClassMethods
      def configure
        yield self
      end
    end
  end
end
