# frozen_string_literal: true

# rubocop:disable Naming/ConstantName
module DulArgonSkin
  Version = '3.0.21'
end
# rubocop:enable Naming/ConstantName
