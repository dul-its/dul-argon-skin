# frozen_string_literal: true

namespace :dul_argon do
  # rubocop:disable Lint/ConstantDefinitionInBlock
  namespace :hathitrust do
    ETAS_SETTING_NAME = 'etas_enabled'

    desc 'Reload overlap file for emergency access ' \
         'to restricted HathiTrust material'
    # Expects a file with the following TSV format
    # oclc  local_id  item_type access  rights
    # 74  000000008 mono  deny  ic
    # 75  000000009 mono  deny  ic
    task :reload_overlaps, [:file_path] => [:environment] do |_, args|
      return unless args.fetch(:file_path, nil)

      HathitrustOverlap.delete_all

      File.open(args[:file_path], 'r').each do |line|
        oclc, local_id, _item_type, access, _rights = line.split("\t")
        next unless %w[allow deny].include?(access)

        HathitrustOverlap.create!(oclc_number: oclc,
                                  bib_id: "DUKE#{local_id}",
                                  access:)
      end

      puts 'Hathitrust overlap file loaded.'
    end

    desc 'Enable Hathitrust ETAS'
    task enable_etas: :environment do
      if HathitrustOverlap.count < 1000
        puts 'ETAS NOT enabled. HathiTrust overlap file must be loaded first.'
        next
      end

      update_etas_setting('true')

      puts 'ETAS enabled.'
    end

    desc 'Disable Hathitrust ETAS'
    task disable_etas: :environment do
      update_etas_setting('false')

      puts 'ETAS disabled.'
    end

    private

    def update_etas_setting(value)
      if Setting.where(name: ETAS_SETTING_NAME).any?
        setting = Setting.where(name: ETAS_SETTING_NAME).first
        setting.value = value
        setting.save!
      else
        Setting.create!(name: ETAS_SETTING_NAME, value:)
      end
      Rails.cache.delete('hathitrust_etas_enabled')
    end
  end
  # rubocop:enable Lint/ConstantDefinitionInBlock
end
