# copied approach from ArcLight:
# https://gitlab.oit.duke.edu/dul-its/dul-arclight/-/blob/main/config/initializers/rack_attack.rb?ref_type=heads
#
# configured via helm chart values.yaml

Rack::Attack.enabled = !Rails.env.test? # disable in test environment

Rack::Attack.safelist_ip("10.138.5.0/24") # OKD cluster

if ENV['BLOCKLIST_IP'].present?
  ENV.fetch('BLOCKLIST_IP').split(',').each do |addr|
    Rack::Attack.blocklist_ip(addr)

    Rails.logger.info('RACK ATTACK') { "Blocking IP: #{addr}" }
  end
end

#
# Throttle requests by IP
# Set rates in /chart/app/values-{env}.yaml
if ENV['THROTTLE_REQUESTS_BY_IP'].present?
  limit, period = ENV.fetch('THROTTLE_REQUESTS_BY_IP').split(',').map(&:to_i)

  throttle = Rack::Attack.throttle('requests by ip', limit: limit, period: period) do |request|
    request.ip unless ['/assets', '/branding', '/favicon'].any? { |path| request.path.start_with?(path) }
  end

  Rails.logger.info('RACK ATTACK') { "Throttling #{throttle.name}: max #{throttle.limit} requests every #{throttle.period} second(s)." }

  Rack::Attack.throttled_response_retry_after_header = true
end

#
# Logging
#
if Rack::Attack.enabled
  Rails.logger.info('RACK ATTACK') { 'rack-attack is enabled.' }

  subscriber = lambda do |*args|
    notification = ActiveSupport::Notifications::Event.new(*args)
    request = notification.payload[:request]

    return unless request.env['rack.attack.matched']

    rack_attack_info = request.env.select { |key, _val| key.start_with?('rack.attack.') }
    request_info = %w[request_method path query_string referer user_agent].map { |meth| [meth, request.send(meth)] }.to_h
    request_info.merge! rack_attack_info

    Rails.logger.info('RACK ATTACK') { request_info.inspect }
  end

  ActiveSupport::Notifications.subscribe(/rack_attack/, subscriber)
else
  Rails.logger.warn('RACK ATTACK') { 'rack-attack is disabled.' }
end
