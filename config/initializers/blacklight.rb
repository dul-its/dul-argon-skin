# frozen_string_literal: true

Blacklight::Engine.config.blacklight.sms_mappings = {
  'Virgin' => 'vmobl.com',
  'AT&T' => 'mms.att.net',
  'Verizon' => 'vzwpix.com',
  'Nextel' => 'messaging.nextel.com',
  'Sprint' => 'messaging.sprintpcs.com',
  'T Mobile' => 'tmomail.net',
  'Alltel' => 'message.alltel.com',
  'Cricket' => 'mms.mycricket.com',
  'Google Fi' => 'msg.fi.google.com'
}
