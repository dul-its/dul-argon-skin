# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )

# Precompile Autosuggest JS & CSS into isolated files for use by external sites.
Rails.application.config.assets.precompile += %w(
  autosuggest/index.js
  dul_argon/catalog-autosuggest.css
)

# We have to create a custom blacklight.js file to drop BL7's (old) autosuggest.
# This requires us to short-circuit BL7's native asset compilation and customize
# it here. We can probably stop doing this once we're on BL8, if not sooner.
# See documentation at:
# https://github.com/projectblacklight/blacklight/wiki/Using-Sprockets-to-compile-javascript-assets#customization-options
# https://github.com/trln/trln_argon/wiki/Blacklight-7---Argon-2-upgrade-notes#jsassets
bl_dir = Bundler.rubygems.find_name('blacklight').first.full_gem_path
assets_path = File.join(bl_dir, 'app', 'javascript')
Rails.application.config.assets.paths << assets_path
